# -*- coding: utf-8 -*-
"""
Created on Sat Feb 25 14:20:40 2017

@author: ik-be
"""

import cv2
import os
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches



def draw_rectangles_on_image(img_plot, coord_list, color):    
    for (x, y, w, h) in coord_list:
        rect = mpatches.Rectangle((x, y), w, h, fill=False, edgecolor=color, linewidth=4)
        img_plot.add_patch(rect)
    return img_plot
    


head_cascade = cv2.CascadeClassifier('/head-cascade.xml')
back_cascade = cv2.CascadeClassifier('/backnumber-cascade.xml')


row_nr, col_nr = 2, 2
test_image_path = '/test_images_cyclists'
img_names = os.listdir(test_image_path)
fig, ax = plt.subplots(nrows = row_nr, ncols = col_nr, figsize=(16, 10))

for i in range(row_nr):
    for j in range(col_nr):
        print i, j
        image_path = os.path.join(test_image_path, img_names[i*2+j])
        img = cv2.imread(image_path)
        gray_img =  cv2.imread(image_path, False)
        
        faces = head_cascade.detectMultiScale(gray_img, 4, 5)
        ax[i][j] = draw_rectangles_on_image(ax[i][j], faces, '')
        
        back_nrs = back_cascade.detectMultiScale(gray_img, 3, 3, 3)
        ax[i][j] = draw_rectangles_on_image(ax[i][j], back_nrs, 'r')
        
        img_plot = ax[i][j].imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        ax[i][j].set_axis_off()
plt.show()
